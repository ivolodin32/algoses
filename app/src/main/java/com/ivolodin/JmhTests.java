package com.ivolodin;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.SingleShotTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Thread)
@Fork(20)
@Warmup(iterations = 5)
public class JmhTests {

    @Param({
            "100000", "200000", "300000", "400000", "500000", "600000", "700000", "800000"
    })
    public int n;
    public boolean[] a;

    @Setup
    public void setup() {
        a = new boolean[n];
        for (int i = 0; i < n; i++)
            a[i] = true;
    }

    @Benchmark
    public boolean[] calculatePrimeNumbers() {
        for (int i = 2; i * i < n * n; i++) {
            if (a[i]) {
                for (int j = i * i; j < n; j = j + i) {
                    a[j] = false;
                }
            }
        }
        return a;
    }

    public static void main(String[] args) throws IOException{
        org.openjdk.jmh.Main.main(args);
    }
}