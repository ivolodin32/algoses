package com.ivolodin;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        for (int n = 100; n <= 100; n += 10) {

            boolean[] a = new boolean[n];
            int totalWrites = 0;
            for (int i = 0; i < n; i++)
                a[i] = true;

            for (int i = 2; i * i < n * n; i++) {
                totalWrites++;
                if (a[i]) {
                    for (int j = i * i; j < n; j = j + i) {
                        totalWrites++;
                        a[j] = false;
                    }
                }
            }
            System.out.println("N: " + n + " Total writes:" + totalWrites);
        }
//        for (int i = 2; i < n; i++) {
//            if (a[i])
//                System.out.println(i);
//        }
    }
}
